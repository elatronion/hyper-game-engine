# The Basics

## Initialization

Any Hyper Game Engine project follows a four step process:

- Including the engine's core header file
- Creating a window
- Initializing the engine with said window
- Start the engine

```c
#include <HGE/HGE_Core.h> // Include The Engine's Core

int main() {
	// Create A Window With Title, Width, Height
	hge_window window = { "Hyper Game Engine", 800, 600 };
	// Initialize Parts Of The Engine Using Our Created Window, Running At 60fps
	hgeInit(60, window, HGE_INIT_RENDERING | HGE_INIT_ECS);

	// ...

	// Start The Game/Engine
	hgeStart();
}
```

## Compiling

```bash
gcc main.c -lhypergameengine
```

## Creating Components

This is what will hold all of the data attached to any entity and going through any system.
Components are nothing more than the data, therefore they can simply be created as a struct!

```c
typedef struct {
    hge_vec2 movement_vector; // Direction
	float speed; // Speed
} character_component;
```

But to actually use this data as a component, you must merge your struct with a base hge_component struct. This can simply be done as shown.

```c
// Create And Initialize Struct
character_component character_data;
character_data.movement_vector.x = 0.0f;
character_data.movement_vector.x = 0.0f;
character_data.movement_vector.speed = 0.0f;
// Convert Struct Into hge_component
hge_component character = hgeCreateComponent("Character", &character_data, sizeof(character_data));
```

"hgeCreateComponent" will return your hge_component with everything setup for you!
You will notice that you must give your component a name, this is for querying.

## Creating Entities

An entity is assembled from different common or unique components.

```c
// Create Entity Shell
hge_entity* entity = hgeCreateEntity();
// Create And Initialize character_component Struct
character_component character_data;
character_data.movement_vector.x = 0.0f;
character_data.movement_vector.x = 0.0f;
character_data.movement_vector.speed = 0.0f;
// Attach Character Component
hgeAddComponent(entity, hgeCreateComponent("Character", &character_data, sizeof(character_data)));
// Create And Initialize hge_vec2 Struct
hge_vec2 position = {0.0f, 0.0f};
// Attach Character Component
hgeAddComponent(entity, hgeCreateComponent("Position", &position, sizeof(position)));
```

This will create an entity that has both a 2d position component and a character component.
Alone, this is nothing more than a grouping of data. Systems will allow us to find any entities that have a certain set of components and run them through a system!

## Creating Systems

This is where all the logic happens!
Creating systems is as easy as creating a function!

```c
void CharacterRenderingSystem(hge_entity* e,
character_component* character, hge_vec3* position, hge_vec3* scale) {
	// Render Character Sprite At Position With Scale
	hgeRenderSprite(hgeResourcesQueryShader("basic"),
			hgeResourcesQueryTexture("character_texture"),
			*position, *scale, 0.0f);
}
```

This function will draw a sprite using a certain character texture and the giving position and scale.
But, how will this function get called?
Well, we tell the engine that it's a system with the following code.

```c
// Create a system that will call "CharacterRenderingSystem"
// for every entity that has all three components: Character, Position and Scale
hgeAddSystem(CharacterRenderingSystem, 3, "Character", "Position", "Scale");
```

Not bad, right?
hgeAddSystem is a variadic, so you can pass as many argument to it as you want!
The first argument is the function that will be called, the second is the number of needed components and finally the names of the aforementioned components.

It is important, when adding your system, to pass all the component names in the order of which they will be passed as arguments to your function!

## Loading Resources

You may have noticed that we used "hgeResourcesQueryShader" and "hgeResourcesQueryTexture" in our system, these are resources!

Resources are any loaded material: textures/sprites, shaders and audio samples!
The resource manager is really easy to interface with, each resources type has two functions. One to load and the other to use.

```c
// Load the image at path "res/character_ball.png" and name it "character_texture" (for future query)
hgeResourcesLoadTexture("res/character_ball.png", "character ball");

// ...

// Access loaded image
hgeResourcesQueryTexture("character ball");
```

## Basic Scene

So, let's put everything together by creating a character that will move around the screen with "WASD".

This character will use an image "res/character_ball.png" and have some momentum.

```c
#include <HGE/HGE_Core.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>

// Components //
typedef enum {
	CHARACTER_IDLE, CHARACTER_MOVING, CHARACTER_DEAD
} character_state;

typedef struct {
	character_state state;
	hge_vec2 movement_vector;
	hge_vec2 movement_direction;
	float speed;
} character_component;

/// ---- /// ---- /// ---- /// ---- /// ---- /// ---- /// ---- /// ---- /// ---- ///

// Systems //

void PlayerCharacterControlSystem(hge_entity* e, tag_component* playable, character_component* character) {
	bool RIGHT = hgeInputGetKey(HGE_KEY_D);
	bool LEFT = hgeInputGetKey(HGE_KEY_A);
	bool UP = hgeInputGetKey(HGE_KEY_W);
	bool DOWN = hgeInputGetKey(HGE_KEY_S);

	hge_vec2 direction_vector = {0, 0};

	switch(character->state) {
		case CHARACTER_IDLE:
			if (RIGHT || LEFT || UP || DOWN) character->state = CHARACTER_MOVING;
			break;
		case CHARACTER_MOVING:
			if (!(RIGHT || LEFT || UP || DOWN)) character->state = CHARACTER_IDLE;

			if(RIGHT) {
				direction_vector.x = 1;
			} if(LEFT) {
				direction_vector.x = -1;
			} if(UP) {
				direction_vector.y = 1;
			} if(DOWN) {
				direction_vector.y = -1;
			}

			direction_vector = hgeMathVec2Normalize(direction_vector);

			character->movement_direction = direction_vector;
			break;
	}
}


void CharacterSystem(hge_entity* e, character_component* character, hge_vec3* position) {
	float acceleration_speed = 10.0f;

	switch(character->state) {
		case CHARACTER_IDLE:
			character->movement_vector.x += (0 - character->movement_vector.x) * acceleration_speed * hgeDeltaTime();
			character->movement_vector.y += (0 - character->movement_vector.y) * acceleration_speed * hgeDeltaTime();
			break;
		case CHARACTER_MOVING:
			character->movement_vector.x += (character->movement_direction.x*character->speed - character->movement_vector.x) * acceleration_speed * hgeDeltaTime();
			character->movement_vector.y += (character->movement_direction.y*character->speed - character->movement_vector.y) * acceleration_speed * hgeDeltaTime();
			break;
	}

	position->x += character->movement_vector.x * hgeDeltaTime();
	position->y += character->movement_vector.y * hgeDeltaTime();

	hge_vec3 character_scale = {8, 8, 0};
	hgeRenderSprite(hgeResourcesQueryShader("basic"), hgeResourcesQueryTexture("character_texture"), *position, character_scale, 0.0f);
}

int main()
{
	// Initialize The Engine
	hge_window window = { "Hyper Game Engine", 800, 600 };
	hgeInit(60, window, HGE_INIT_RENDERING | HGE_INIT_ECS);

	// Load Resources
	hgeResourcesLoadTexture("res/character_ball.png", "character_texture");

	// ECS //

	// Systems //
	hgeAddBaseSystems();
	hgeAddSystem(PlayerCharacterControlSystem, 2, "Playable", "Character");
	hgeAddSystem(CharacterSystem, 2, "Character", "Position");

	// Entities //
	// Camera
	hge_transform camera_transform;
    camera_transform.position = hgeVec3(0, 0, 0);
    camera_transform.scale = hgeVec3(0, 0, 0);
    camera_transform.rotation = hgeQuaternionInitRotation(hgeVec3(0, 1, 0), hgeRadians(180));
	hge_entity* camera_entity = hgeCreateCamera(
        camera_transform,
        hgeVec2(hgeWindowWidth(), hgeWindowHeight()),
        1/6.f,
        -100.f, 100.f,
        HGE_CAMERA_ORTHOGRAPHIC
    );
	tag_component active_tag;
	hgeAddComponent(camera_entity, hgeCreateComponent("active", &active_tag, sizeof(active_tag)));

	// Player Character
	hge_entity* player_character_entity = hgeCreateEntity();
	hge_vec3 player_character_position = {0, 0, 0};
	hgeAddComponent(player_character_entity, hgeCreateComponent("Position", &player_character_position, sizeof(player_character_position)));
	tag_component playable;
	hgeAddComponent(player_character_entity, hgeCreateComponent("Playable", &playable, sizeof(playable)));
	character_component player_character_component;
	player_character_component.state = CHARACTER_IDLE;
	player_character_component.movement_vector.x = 0.0f;
	player_character_component.movement_vector.y = 0.0f;
	player_character_component.speed = 120.0f;
	hgeAddComponent(player_character_entity, hgeCreateComponent("Character", &player_character_component, sizeof(player_character_component)));

	// Start The Game/Engine
	hgeStart();

	return 0;
}
```

## Want to learn more?

Learn how to use [the networking engine](Networking.md)!