# Networking

## Initialization

```c
#include <HGE/HGE_Core.h> // Include The Engine's Core

int main() {
	// Create A Window With Title, Width, Height
	hge_window window = { "Hyper Game Engine", 800, 600 };
	// Initialize Parts Of The Engine Using Our Created Window, Running At 60fps
	hgeInit(60, window, HGE_INIT_RENDERING | HGE_INIT_ECS | HGE_INIT_NETWORKING);
    // flag 'HGE_INIT_ALL' does the equivalent

	// ...

	// Start The Game/Engine
	hgeStart();
}
```

## Getting Connected

### Hosting

```c
// Host a server using port 7777, max 32 clients
hgeNetworkHost(7777, 32);
```

### Connecting

```c
// Connect to server "usergames.net" port 7777
hgeNetworkConnect("usergames.net", 7777);
```

## Sending Packets

### Server

```c
// Send Packet "Hello, World!\0" of size 14
// to all connected peers
hgeNetworkBroadcastPacket("Hello, World!\0", 14);
```

### Client

```c
// Send Packet "Hello, World!\0" if size 14
// to server
hgeNetworkSendPacket("Hello, World!\0", 14);
```

## Receiving Packets

As of now, this process must be done manually by the developer.
This is sure to change in the future.

```c
// Get all currently received packets
hge_network_events* eventlist = hgeNetworkEventList();
// Loop through all network events
for(int i = 0; i < eventlist->num_messages; i++) {
	// Process information as desired
	printf("%s\n", eventlist->messages[i].data);
	// Free data when no longer needed
	free(eventlist->messages[i].data);
}
// Once all data has been processed, set the number of messages to 0
eventlist->num_messages = 0;
```

## Complete Example

```c
#include <HGE/HGE_Core.h>

void NetworkingSystem(hge_entity entity, tag_component network_manager) {
	hge_network_events* eventlist = hgeNetworkEventList();
	for(int i = 0; i < eventlist->num_messages; i++) {
		printf("%s\n", eventlist->messages[i].data);
		free(eventlist->messages[i].data);
	}
	eventlist->num_messages = 0;
}

int main(int argc, char *argv[]) {
	bool is_host = false;

	if(argc>1)
		if(strcmp(argv[1], "host") == 0)
			is_host = true;

	hge_window window = { "Hello, World - Networking Test", 800, 600 };
	hgeInit(60, window, HGE_INIT_ALL);

	hgeAddSystem(NetworkingSystem, 1, "NetworkManager");

	// Network Entity, will run NetworkingSystem every frame
	hge_entity* networking_manager_entity = hgeCreateEntity();
	tag_component networking_manager_tag;
	hgeAddComponent(networking_manager_entity, hgeCreateComponent("NetworkManager", &networking_manager_tag, sizeof(networking_manager_tag)));

	printf("%s\n", is_host ? "You Are Hosting!" : "You Are Connecting!");
	if(is_host) {
		hgeNetworkHost(7777, 32);
	} else {
		hgeNetworkConnect("usergames.net", 7777);
		hgeNetworkSendPacket("Hello, World!\0", 14);
	}

	hgeStart();
}
```
