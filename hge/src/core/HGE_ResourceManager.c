#include "HGE_ResourceManager.h"
#include "HGE_FileUtility.h"
#include "HGE_Log.h"

int num_shaders = 0;
hge_resource_shader shaders[HGE_MAX_SHADERS];
int num_textures = 0;
hge_resource_texture textures[HGE_MAX_TEXTURES];
int num_meshes = 0;
hge_resource_mesh meshes[HGE_MAX_MESHES];
int num_audiosamples = 0;
hge_resource_audiosample audiosamples[HGE_MAX_AUDIOSAMPLES];

hge_resource_shader* hgeResourcesGetShaderArray() { return &shaders; }

hge_resource_mesh* hgeResourcesGetMeshArray() { return &meshes; }

int hgeResourcesNumShaders() { return num_shaders; }
int hgeResourcesNumTextures() { return num_textures; }
int hgeResourcesNumMeshes() { return num_meshes; }

void hgeResourcesLoadShader(const char* vertex_path, const char* geometry_path, const char* fragment_path, const char* name) {
  // Make Sure We Have Enough Space In The Array
  if(num_shaders > HGE_MAX_SHADERS) {
    HGE_WARNING("shader cannot be loaded, max capacity reached!");
    return;
  }
  // Load And Store Shader
  shaders[num_shaders].name = name;
  shaders[num_shaders].shader = hgeLoadShader(vertex_path, geometry_path, fragment_path);
  num_shaders++;
}

void hgeResourcesLoadTexture(const char* path, const char* name) {
  // Make Sure We Have Enough Space In The Array
  if(num_textures > HGE_MAX_TEXTURES) {
    HGE_WARNING("texture cannot be loaded, max capacity reached!");
    return;
  }
  // Load And Store Texture
  textures[num_textures].name = name;
  textures[num_textures].texture = hgeLoadTexture(path);
  num_textures++;
}

void hgeResourcesLoadMesh(const char* path, const char* name) {
  // Make Sure We Have Enough Space In The Array
  if(num_meshes > HGE_MAX_MESHES) {
    HGE_WARNING("mesh cannot be loaded, max capacity reached!");
    return;
  }
  // Load And Store Texture
  meshes[num_meshes].name = name;
  meshes[num_meshes].mesh = hgeLoadMesh(path);
  num_meshes++;
}

void hgeResourcesLoadAudio(const char* path, const char* name) {
  // Make Sure We Have Enough Space In The Array
  if(num_meshes > HGE_MAX_AUDIOSAMPLES) {
    HGE_WARNING("sample cannot be loaded, max capacity reached!");
    return;
  }
  // Load And Store Texture
  audiosamples[num_audiosamples].name = name;
  audiosamples[num_audiosamples].sample = hgeLoadAudioFile(path);
  num_audiosamples++;
}


hge_shader hgeResourcesQueryShader(const char* name) {
  for(int i = 0; i < num_shaders; i++) {
    if(!strcmp(shaders[i].name, name)) {
			return shaders[i].shader;
		}
  }
}

hge_texture hgeResourcesQueryTexture(const char* name) {
  for(int i = 0; i < num_textures; i++) {
    if(!strcmp(textures[i].name, name)) {
			return textures[i].texture;
		}
  }
  hge_texture null = { -1 };
  return null;
}

hge_mesh hgeResourcesQueryMesh(const char* name) {
  for(int i = 0; i < num_meshes; i++) {
    if(!strcmp(meshes[i].name, name)) {
			return meshes[i].mesh;
		}
  }
  hge_mesh null = { };
  return null;
}

hge_audiosample hgeResourcesQueryAudio(const char* name) {
  for(int i = 0; i < num_audiosamples; i++) {
    if(!strcmp(audiosamples[i].name, name)) {
			return audiosamples[i].sample;
		}
  }
  hge_audiosample null = { };
  return null;
}

void hgeResourcesLoadDefaults() {
  meshes[num_meshes].name = "HGE QUAD";
  meshes[num_meshes].mesh = hgeGenerateQuad();
  num_meshes++;
}

hgeResourcesClean() {
  for(int i = 0; i < num_audiosamples; i++) {
    hgeDestroyAudioSample(audiosamples[i].sample);
  }
}
