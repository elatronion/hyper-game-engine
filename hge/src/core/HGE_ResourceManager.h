#ifndef HGE_RESOURCE_MANAGER_H
#define HGE_RESOURCE_MANAGER_H

#define HGE_MAX_TEXTURES 1000
#define HGE_MAX_MESHES 1000
#define HGE_MAX_AUDIOSAMPLES 500
#define HGE_MAX_SHADERS 1000

#include "HGE_Shader.h"
#include "HGE_Texture.h"
#include "HGE_Mesh.h"
#include "HGE_AudioSample.h"

typedef struct {
  const char* name;
  hge_shader shader;
} hge_resource_shader;

typedef struct {
  const char* name;
  hge_texture texture;
} hge_resource_texture;

typedef struct {
  const char* name;
  hge_mesh mesh;
} hge_resource_mesh;

typedef struct {
  const char* name;
  hge_audiosample sample;
} hge_resource_audiosample;

void hgeResourcesLoadShader(const char* vertex_path, const char* geometry_path, const char* fragment_path, const char* name);
void hgeResourcesLoadTexture(const char* path, const char* name);
void hgeResourcesLoadMesh(const char* path, const char* name);
void hgeResourcesLoadAudio(const char* path, const char* name);

hge_shader hgeResourcesQueryShader(const char* name);
hge_texture hgeResourcesQueryTexture(const char* name);
hge_mesh hgeResourcesQueryMesh(const char* name);
hge_audiosample hgeResourcesQueryAudio(const char* name);

hge_resource_shader* hgeResourcesGetShaderArray();
hge_resource_mesh* hgeResourcesGetMeshArray();

int hgeResourcesNumShaders();
int hgeResourcesNumTextures();
int hgeResourcesNumMeshes();

void hgeResourcesLoadDefaults();

void hgeResourcesClean();

#endif
