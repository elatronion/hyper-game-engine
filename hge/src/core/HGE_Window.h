#ifndef HGE_WINDOW_H
#define HGE_WINDOW_H

#include <stdbool.h>
#include <stdint.h>

typedef struct {
  const char* title;
  int width, height;
} hge_window;

int hgeCreateWindow(const char* title, int x, int y, int width, int height, uint8_t flags);
void hgeDestroyWindow();

unsigned int hgeWindowWidth();
unsigned int hgeWindowHeight();

void hgeProcessInput();
void hgeSwapBuffers();
void hgePollEvents();

bool hgeWindowIsClosedRequested();

#endif
