#include "HGE_Core.h"
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <GLFW/glfw3.h> // glfwGetTime

int ticks = 0;
float timer = 0;
int FPS;
int TICKS_PER_FRAME;

bool running = false;

void hgeInit(int frame_rate, hge_window window, uint8_t flags) {
  HGE_LOG("initializing core");
  FPS = frame_rate;
  TICKS_PER_FRAME = 1000 / FPS;
  if(hgeCreateWindow(window.title, 0, 0, window.width, window.height, NULL))
    exit(1);
  if(flags & HGE_INIT_RENDERING) hgeRenderInit(window);
  if(flags & HGE_INIT_NETWORKING) hgeNetworkInit();
  if(flags & HGE_INIT_ECS) hgeECSInit();
  if(flags & HGE_INIT_AUDIO) hgeAudioInit();

  hgeResourcesLoadDefaults();

  HGE_SUCCESS("initialized core");
}

void hgeCleanUp() {
  HGE_LOG("cleaning up core");
  hgeNetworkCleanUp();
  hgeECSCleanUp();
  hgeDestroyWindow();
  hgeRenderingEngineCleanUp();
  hgeAudioCleanUp();
  hgeResourcesClean();
  HGE_SUCCESS("cleaned up core");
}

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

float hgeDeltaTime() { return deltaTime; }
float hgeRuntime() { return glfwGetTime(); }
int hgeTicks() { return ticks; }

void hgeWait(unsigned int ms) {
  usleep(ms*1000);
}

void hgeStart() {
  running = true;

  int i = 0;
  while (running) {
    float currentFrame = hgeRuntime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    if(hgeWindowIsClosedRequested())
      hgeStop();

    hgeWorldUpdate();
    hgeNetworkPoll();
    hgeRenderFrame();

    hge_entity* active_camera_entity = hgeQueryEntity(1, "active");
    hge_camera* active_camera = active_camera_entity->components[hgeQuery(active_camera_entity, "camera")].data;
  	hgeRenderFramebufferToScreen(active_camera->framebuffer);

    hgeSwapBuffers();
    hgePollEvents();

    ticks++;

    timer += deltaTime;
    if(timer >= 2.f) {
      printf("fps: %dfps\n", (int)(1.f/deltaTime));
      printf("deltaTime: %fms\n\n", deltaTime);
      timer = 0;
    }

    //If frame finished early
    if( deltaTime < TICKS_PER_FRAME )
    {
        //Wait remaining time
        hgeWait(TICKS_PER_FRAME - deltaTime);
    }
  }
  hgeCleanUp();
}

void hgeStop() { running = false; }
