#ifndef HGE_LOG_H
#define HGE_LOG_H

#include "HGE_Log.h"
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

char* concat(const char *str1, const char *str2) {
    char *result = malloc(strlen(str1) + strlen(str2) + 1); // +1 for the null-terminator
    // probably want to check for errors in malloc here
    strcpy(result, str1);
    strcat(result, str2);
    return result;
}

void hgeAddToLogFile(const char* message) {
  time_t rawtime; struct tm * timeinfo; time ( &rawtime );
  timeinfo = localtime ( &rawtime );

  char* date_str = malloc(255);
  strcpy(date_str, asctime(timeinfo));
  date_str[strlen(date_str)-6] = '\0';

  FILE* log_file=fopen("hge.log", "a");
  fprintf(log_file, "%s %s\n", date_str, message); fclose(log_file);
}

#endif
