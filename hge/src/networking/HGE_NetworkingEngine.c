#include "HGE_NetworkingEngine.h"
#include "HGE_Log.h"

#include <stdio.h>
#include <stdbool.h>
#include <enet/enet.h>

hge_network_events eventlist;
hge_network_events* hgeNetworkEventList(){ return &eventlist; }

int num_connections = 0;

ENetHost* host;
ENetPeer* peer;

bool hosting = false;
bool connected = false;

bool hgeNetworkIsHost() { return hosting; }
bool hgeNetworkIsConnected() { return connected; }

int hgeNetworkInit() {
  HGE_LOG("initializing networking engine");
  if(enet_initialize() != 0)
  {
    HGE_ERROR("an error occurred while initializing ENet");
    return EXIT_FAILURE;
  }
  atexit(enet_deinitialize);
  HGE_SUCCESS("initialized networking engine");
}

int hgeNetworkHost(unsigned int port, unsigned int max_connections) {
  ENetAddress address;
  ENetEvent event;

  address.host = ENET_HOST_ANY;
  address.port = port;

  host = enet_host_create (&address	/* the address to bind the server host to */,
  				max_connections	/* allow up to max_connections clients and/or outgoing connections */,
  				1	/* allow up to 1 channel to be used. */,
  				0	/* assume any amount of incoming bandwidth */,
  				0	/* assume any amount of outgoing bandwidth */);

  if(host == NULL)
  {
    HGE_ERROR("an error occurred while trying to create an ENet server host");
    return EXIT_FAILURE;
  }

  hosting = true;
  connected = true;
  return EXIT_SUCCESS;
}

int hgeNetworkConnect(const char* address_str, unsigned int port) {
  ENetAddress address;
  ENetEvent event;

  host = enet_host_create(NULL, 1, 1, 0, 0);

  if(host == NULL)
  {
    HGE_ERROR("an error occurred while trying to create an ENet client host!");
    return EXIT_FAILURE;
  }

  enet_address_set_host(&address, address_str);
  address.port = port;

  peer = enet_host_connect(host, &address, 1, 0);
  if(peer == NULL)
  {
    HGE_ERROR("no available peers for initiating an ENet connection!");
  	return EXIT_FAILURE;
  }

  if(enet_host_service(host, &event, 5000) > 0 &&
  	 event.type == ENET_EVENT_TYPE_CONNECT)
  {
    HGE_SUCCESS("connection succeeded");
    connected = true;
  }
  else
  {
  	enet_peer_reset(peer);
    HGE_WARNING("connection failed");
  	return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

void hgeNetworkPoll() {
  if(!connected) return;
  ENetEvent event;
  /* Wait up to 0 milliseconds for an event. */
  while(enet_host_service(host, &event, 0) > 0)
  {
  	switch(event.type)
  	{
      case ENET_EVENT_TYPE_CONNECT:
        printf ("A new client connected from %x:%u.\n",
        event.peer -> address.host,
        event.peer -> address.port);

        num_connections++;

        // Allocates storage
        event.peer -> data = (char*)malloc(255 * sizeof(char));
        sprintf(event.peer -> data, "%x:%u", event.peer -> address.host, event.peer -> address.port);
        break;
  		case ENET_EVENT_TYPE_RECEIVE:
        eventlist.messages[eventlist.num_messages].sender.enet_peer = event.peer;
        eventlist.messages[eventlist.num_messages].size = event.packet -> dataLength;
        eventlist.messages[eventlist.num_messages].data = (char*)malloc(event.packet -> dataLength * sizeof(char));
        // Copy Data Into Buffer
        for(int i = 0; i < event.packet -> dataLength; i++) {
          eventlist.messages[eventlist.num_messages].data[i] = event.packet->data[i];
        }
        eventlist.num_messages++;

        /* Clean up the packet now that we're done using it. */
        enet_packet_destroy (event.packet);
  			break;
      case ENET_EVENT_TYPE_DISCONNECT:
        printf ("%s disconnected.\n", event.peer -> data);
        num_connections--;

        /* Reset the peer's client information. */
        free(event.peer -> data);
        event.peer -> data = NULL;
        break;
  	}
  }
}

void hgeNetworkBroadcastPacket(const char* data, size_t size) {
  ENetPacket* packet = enet_packet_create(data, size, ENET_PACKET_FLAG_RELIABLE);
  // Send the packet to all connected peers on channel 0
  enet_host_broadcast(host, 0, packet);
}

void hgeNetworkSendPacket(/*ENetPeer* peer, */const char* data, size_t size) {
  ENetPacket* packet = enet_packet_create(data, size, ENET_PACKET_FLAG_RELIABLE);
  // Send the packet to the peer on channel 0
  enet_peer_send(peer, 0, packet);
}

void hgeNetworkSendPacketTo(hge_network_peer network_peer, const char* data, size_t size) {
  ENetPacket* packet = enet_packet_create(data, size, ENET_PACKET_FLAG_RELIABLE);
  // Send the packet to the peer on channel 0
  ENetPeer* n_peer = network_peer.enet_peer;
  printf("Sending Packet To %s!\n", n_peer->data);
  enet_peer_send(n_peer, 0, packet);
}

void hgeNetworkDisconnect() {
  if(!connected || hosting) return;
  ENetEvent event;

  enet_peer_disconnect(peer, 0);

  /* Wait up to 1000 milliseconds for an event. */
  while(enet_host_service(host, &event, 1000) > 0)
  {
  	switch(event.type)
  	{
  		case ENET_EVENT_TYPE_RECEIVE:
  			enet_packet_destroy(event.packet);
  			break;
  		case ENET_EVENT_TYPE_DISCONNECT:
  			puts("Disconnection succeeded.");
        connected = false;
  			break;
  	}
  }
  connected = false;
}

void hgeNetworkCleanUp()
{
  hgeNetworkDisconnect();
  enet_host_destroy(host);
}
