#include "HGE_FileUtility.h"
#include "HGE_Log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/SOIL.h>

char* hgeLoadFileAsString(const char* path) {
  printf("Loading: %s\n", path);
  char * buffer = 0;
  long length;
  FILE * file = fopen (path, "rb");

  if (file)
  {
    fseek (file, 0, SEEK_END);
    length = ftell (file);
    fseek (file, 0, SEEK_SET);
    buffer = malloc (length + 1);
    buffer[length] = '\0';
    if (buffer)
    {
      fread (buffer, 1, length, file);
    }
    fclose (file);
  }

  if (buffer)
  {
    return buffer;
  } else {
    HGE_WARNING("could not load file");
    return NULL;
  }
}

hge_texture hgeLoadTexture(const char* path) {
  hge_texture texture;
  // Load image
  int width, height;
  unsigned char* image = SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGBA);
  // Now generate texture
  texture = hgeGenerateTexture(width, height, image);
  // And finally free image data
  SOIL_free_image_data(image);
  return texture;
}

int convertToInt(char* buffer, int len)
{
	int a;

	if (!hgeIsBigEndian())
	for (int i = 0; i < len; i++)
		((char*)&a)[i] = buffer[i];
	else
	for (int i = 0; i < len; i++)
		((char*)&a)[3-i] = buffer[i];

	return a;
}

char* hgeLoadWAV(const char* fn, int* chan, int* samplerate, int* bps, int* size)
{
  char buffer[4];
  FILE* file = fopen(fn, "rb");
  fread(buffer, 4, 1, file);
  if (strncmp(buffer, "RIFF", 4) != 0)
	{
    printf("\"%s\" Is Not A Valid WAV File\n", fn);
	}
  fread(buffer, 4, 1, file);
  fread(buffer, 4, 1, file); // WAVE
  fread(buffer, 4, 1, file); // fmt
  fread(buffer, 4, 1, file); // 16
  fread(buffer, 2, 1, file); // 1
  fread(buffer, 2, 1, file);
  *chan = convertToInt(buffer, 2);
  fread(buffer, 4, 1, file);
  *samplerate = convertToInt(buffer, 4);
  fread(buffer, 4, 1, file);
  fread(buffer, 2, 1, file);
  fread(buffer, 2, 1, file);
  *bps = convertToInt(buffer, 2);
  fread(buffer, 4, 1, file); // data
  fread(buffer, 4, 1, file);
  *size = convertToInt(buffer, 4);
  char* data = malloc(*size);
  fread(data, *size, 1, file);

  fclose(file);

	return data;
}

bool hgeIsBigEndian()
{
	int a = 1;
	return !((char*)&a)[0];
}
