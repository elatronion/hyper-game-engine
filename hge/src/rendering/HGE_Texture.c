#include "HGE_Texture.h"
#include <glad/glad.h>

hge_texture hgeGenerateTexture(int width, int height, unsigned char* data) {
  hge_texture texture;

  // Format of texture object
  GLuint Internal_Format = GL_RGBA;
  // Format of loaded image
  GLuint Image_Format = GL_RGBA;

  GLuint Wrap_S = GL_REPEAT;
  GLuint Wrap_T = GL_REPEAT;

  glGenTextures(1, &texture.id);
	texture.width = width;
	texture.height= height;

	// Create Texture
	glBindTexture(GL_TEXTURE_2D, texture.id);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, Internal_Format, texture.width, texture.height, 0, Image_Format, GL_UNSIGNED_BYTE, data);
	// Set Texture wrap and filter modes
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, Wrap_S);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, Wrap_T);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


	// TODO: Anisotropic Filtering

	// Unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

  return texture;
}

hge_texture hgeSetTexture(hge_texture texture, unsigned int gltexture, int width, int height)
{
  hge_texture new_texture = texture;
	new_texture.id = gltexture;
	new_texture.width = width;
	new_texture.height = height;
  return new_texture;
}

void hgeActiveTexture(uint32_t texture_id) {
  glActiveTexture(GL_TEXTURE0 + texture_id);
}

void hgeBindTexture(hge_texture texture) {
	glBindTexture(GL_TEXTURE_2D, texture.id);
}
