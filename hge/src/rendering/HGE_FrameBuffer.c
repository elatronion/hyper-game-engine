#include "HGE_FrameBuffer.h"
#include <glad/glad.h>

void createMultisampleColorAttachment(hge_framebuffer* framebuffer) {
  glGenRenderbuffers(1, &framebuffer->colorBuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, framebuffer->colorBuffer);
  glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_RGBA8, framebuffer->Width, framebuffer->Height);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, framebuffer->colorBuffer);
}

hge_framebuffer hgeGenerateFramebuffer(GLuint width, GLuint height, bool multisampled, bool linear) {
  hge_framebuffer s_framebuffer;
  s_framebuffer.multisample = multisampled;
  s_framebuffer.Width = width;
  s_framebuffer.Height = height;
  /*// Create Texture
  glBindTexture(GL_TEXTURE_2D, this->ID);
  glTexImage2D(GL_TEXTURE_2D, 0, this->Internal_Format, width, height, 0, this->Image_Format, GL_UNSIGNED_BYTE, data);
  // Set Texture wrap and filter modes
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, this->Wrap_S);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, this->Wrap_T);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, this->Filter_Min);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, this->Filter_Max);
  // Unbind texture
  glBindTexture(GL_TEXTURE_2D, 0);
  //glScalef(1.0f/8.0, 1.0f/3.0f, 1.0f);*/

  glGenFramebuffers(1, &s_framebuffer.framebuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, s_framebuffer.framebuffer);

  // Generate texture

  if (!multisampled)
  {
    glGenTextures(1, &s_framebuffer.colorBuffer);
    glBindTexture(GL_TEXTURE_2D, s_framebuffer.colorBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    if(linear) {
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else {
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    s_framebuffer.color = hgeSetTexture(s_framebuffer.color, s_framebuffer.colorBuffer, width, height);
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  else
  {
    createMultisampleColorAttachment(&s_framebuffer);
    glBindTexture(GL_TEXTURE_2D, 0);
    s_framebuffer.color = hgeSetTexture(s_framebuffer.color, s_framebuffer.colorBuffer, width, height);
  }

  // Attach it to currently bound framebuffer object
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, s_framebuffer.colorBuffer, 0);

  GLuint rbo;
  glGenRenderbuffers(1, &rbo);
  glBindRenderbuffer(GL_RENDERBUFFER, rbo);
  if (!multisampled)
  {
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
  }
  else
  {
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH24_STENCIL8, width, height);
  }
  //glBindRenderbuffer(GL_RENDERBUFFER, 0);

  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    printf("ERROR::FRAMEBUFFER:: Framebuffer is not complete!\n");

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  return s_framebuffer;
}

void hgeResolveToFbo(hge_framebuffer framebuffer, hge_framebuffer* outputFbo) {
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, outputFbo->framebuffer);
  glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer.framebuffer);
  glBlitFramebuffer(0, 0, framebuffer.Width, framebuffer.Height, 0, 0, outputFbo->Width, outputFbo->Height, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
  hgeUnbindFramebuffer();
}

void hgeBindFramebuffer(hge_framebuffer framebuffer) {
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.framebuffer);
}

void hgeUnbindFramebuffer() {
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
