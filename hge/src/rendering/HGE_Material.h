#ifndef HGE_MATERIAL_H
#define HGE_MATERIAL_H

#include "HGE_Texture.h"
#include <stdbool.h>
#include "HGE_Math3D.h"

typedef struct {
  hge_texture diffuse, normal;

  bool lit;
  hge_vec4 color_multiplier;
} hge_material;

#endif
