#include "HGE_Mesh.h"
#include <stdio.h>
#include <stdlib.h>
#include "HGE_Math3D.h"
#include <glad/glad.h>

#if __linux__
#else
size_t getline(char **lineptr, size_t *n, FILE *stream) {
    char *bufptr = NULL;
    char *p = bufptr;
    size_t size;
    int c;

    if (lineptr == NULL) {
        return -1;
    }
    if (stream == NULL) {
        return -1;
    }
    if (n == NULL) {
        return -1;
    }
    bufptr = *lineptr;
    size = *n;

    c = fgetc(stream);
    if (c == EOF) {
        return -1;
    }
    if (bufptr == NULL) {
        bufptr = malloc(128);
        if (bufptr == NULL) {
            return -1;
        }
        size = 128;
    }
    p = bufptr;
    while(c != EOF) {
        if ((p - bufptr) > (size - 1)) {
            size = size + 128;
            bufptr = realloc(bufptr, size);
            if (bufptr == NULL) {
                return -1;
            }
        }
        *p++ = c;
        if (c == '\n') {
            break;
        }
        c = fgetc(stream);
    }

    *p++ = '\0';
    *lineptr = bufptr;
    *n = size;

    return p - bufptr - 1;
}
#endif

typedef struct {
  hge_vec3 position;
  hge_vec2 texture_coord;
  hge_vec3 normal;
} hge_vertex;

typedef struct element_node {
    unsigned int index;
    struct element_node * next;
} element_node_t;

unsigned int getElementAtIndex(element_node_t* head, unsigned int index) {
  element_node_t* cur = head->next;
  unsigned int cur_index = 0;
  while(cur) {
    if(cur_index == index) {
        return cur->index;
        break;
    }
    cur = cur->next;
    cur_index++;
  }

  printf("Couldn't Find [%u] Position For Element Node!\n", index);
  return 0;
}

unsigned int lengthOfElementList(element_node_t* head) {
  element_node_t* cur = head->next;
  unsigned int length = 0;
  while(cur) {
    cur = cur->next;
    length++;
  }
  return length;
}

void pushElementToList(element_node_t* head, unsigned int index) {
  element_node_t* cur = head;

  while(cur->next)
    cur = cur->next;

  cur->next = (element_node_t *) malloc(sizeof(element_node_t));
  cur = cur->next;
  cur->index = index;
  cur->next = NULL;
}

typedef struct vert_node {
    hge_vertex vertex;
    struct vert_node * next;
} vert_node_t;

unsigned int lengthOfVertexList(vert_node_t* head) {
  vert_node_t* cur = head->next;
  unsigned int length = 0;
  while(cur) {
    cur = cur->next;
    length++;
  }
  return length;
}

vert_node_t* getVertexNodeAtIndex(vert_node_t* head, unsigned int index) {
  vert_node_t* cur = head->next;
  unsigned int cur_index = 0;
  while(cur) {
    if(cur_index == index) {
        return cur;
        break;
    }
    cur = cur->next;
    cur_index++;
  }

  printf("Couldn't Find [%u] Position For Vertex Node!\n", index);
  return NULL;
}

typedef struct vec_node {
    hge_vec3 vec;
    struct vec_node * next;
} vec_node_t;

hge_vec3 getVecAtIndex(vec_node_t* head, unsigned int index) {
  vec_node_t* cur = head->next;
  unsigned int cur_index = 0;
  while(cur) {
    if(cur_index == index) {
        return cur->vec;
        break;
    }
    cur = cur->next;
    cur_index++;
  }

  printf("Couldn't Find [%u] Position For Vec!\n", index);
  hge_vec3 vec;
  return vec;
}

void freeElementTree(element_node_t* head) {
  element_node_t* cur = head->next;
  while(cur) {
    element_node_t* tmp = cur;
    cur = cur->next;
    free(tmp);
  }
  free(head);
}

void freeVertTree(vert_node_t* head) {
  vert_node_t* cur = head->next;
  while(cur) {
    vert_node_t* tmp = cur;
    cur = cur->next;
    free(tmp);
  }
  free(head);
}

void freeVecTree(vec_node_t* head) {
  vec_node_t* cur = head->next;
  while(cur) {
    vec_node_t* tmp = cur;
    cur = cur->next;
    free(tmp);
  }
  free(head);
}

hge_mesh hgeLoadMesh(const char* file_path) {
  FILE * file;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;

  file = fopen(file_path, "r");
  if (file == NULL)
      exit(EXIT_FAILURE);

  hge_mesh mesh;
  vert_node_t* head_verticies = (vert_node_t *) malloc(sizeof(vert_node_t));
  head_verticies->next = NULL;
  vert_node_t* cur_vertex = head_verticies;

  vec_node_t* head_normals = (vec_node_t *) malloc(sizeof(vec_node_t));
  head_normals->next = NULL;
  vec_node_t* cur_normal = head_normals;

  vec_node_t* head_texture_coords = (vec_node_t *) malloc(sizeof(vec_node_t));
  head_texture_coords->next = NULL;
  vec_node_t* cur_texture_coord = head_texture_coords;

  element_node_t* head_elements = (vec_node_t *) malloc(sizeof(element_node_t));;
  head_elements->next = NULL;
  element_node_t* cur_element = head_elements;

  float x, y, z;
  unsigned int v0, v1, v2, t0, t1, t2, n0, n1, n2;
  while ((read = getline(&line, &len, file)) != -1) {
      switch(line[0]) {
        case 'v':
          if(line[1] == 'n') {
            sscanf(line, "%*s %f %f %f", &x, &y, &z);
            cur_normal->next = (vec_node_t *) malloc(sizeof(vec_node_t));
            cur_normal = cur_normal->next;
            cur_normal->vec.x = x;
            cur_normal->vec.y = y;
            cur_normal->vec.z = z;
            cur_normal->next = NULL;
            break;
          }
          else if(line[1] == 't') {
            sscanf(line, "%*s %f %f", &x, &y);
            cur_texture_coord->next = (vec_node_t *) malloc(sizeof(vec_node_t));
            cur_texture_coord = cur_texture_coord->next;
            cur_texture_coord->vec.x = x;
            cur_texture_coord->vec.y = y;
            cur_texture_coord->vec.z = 0.0f;
            cur_texture_coord->next = NULL;
          }
          else if(line[1] != ' ') {
            continue;
          }
          sscanf(line, "%*s %f %f %f", &x, &y, &z);
          cur_vertex->next = (vert_node_t *) malloc(sizeof(vert_node_t));
          cur_vertex = cur_vertex->next;
          cur_vertex->vertex.position.x = x;
          cur_vertex->vertex.position.y = y;
          cur_vertex->vertex.position.z = z;
          cur_vertex->next = NULL;
          break;
        case 'f':
          sscanf(line, "%*c %u/%u/%u %u/%u/%u %u/%u/%u", &v0, &t0, &n0, &v1, &t1, &n1, &v2, &t2, &n2);
          // Offset
          v0--; v1--; v2--;
          t0--; t1--; t2--;
          n0--; n1--; n2--;

          getVertexNodeAtIndex(head_verticies, v0)->vertex.texture_coord.x = getVecAtIndex(head_texture_coords, t0).x;
          getVertexNodeAtIndex(head_verticies, v0)->vertex.texture_coord.y = getVecAtIndex(head_texture_coords, t0).y;
          getVertexNodeAtIndex(head_verticies, v0)->vertex.normal = getVecAtIndex(head_normals, n0);

          getVertexNodeAtIndex(head_verticies, v1)->vertex.texture_coord.x = getVecAtIndex(head_texture_coords, t1).x;
          getVertexNodeAtIndex(head_verticies, v1)->vertex.texture_coord.y = getVecAtIndex(head_texture_coords, t1).y;
          getVertexNodeAtIndex(head_verticies, v1)->vertex.normal = getVecAtIndex(head_normals, n1);

          getVertexNodeAtIndex(head_verticies, v2)->vertex.texture_coord.x = getVecAtIndex(head_texture_coords, t2).x;
          getVertexNodeAtIndex(head_verticies, v2)->vertex.texture_coord.y = getVecAtIndex(head_texture_coords, t2).y;
          getVertexNodeAtIndex(head_verticies, v2)->vertex.normal = getVecAtIndex(head_normals, n2);

          pushElementToList(head_elements, v0);
          pushElementToList(head_elements, v1);
          pushElementToList(head_elements, v2);
          break;
      }
  }

  fclose(file);
  if (line)
      free(line);

  unsigned int length_of_verticies = lengthOfVertexList(head_verticies);
  float* vertexArray = malloc(8 * length_of_verticies * sizeof(float));

  for(int i = 0; i < length_of_verticies; i++) {
    hge_vertex vertex = getVertexNodeAtIndex(head_verticies, i)->vertex;
    vertexArray[i * 8 + 0] = vertex.position.x;
    vertexArray[i * 8 + 1] = vertex.position.y;
    vertexArray[i * 8 + 2] = vertex.position.z;
    vertexArray[i * 8 + 3] = vertex.texture_coord.x;
    vertexArray[i * 8 + 4] = vertex.texture_coord.y;
    vertexArray[i * 8 + 5] = vertex.normal.x;
    vertexArray[i * 8 + 6] = vertex.normal.y;
    vertexArray[i * 8 + 7] = vertex.normal.z;
  }

  unsigned int length_of_elements = lengthOfElementList(head_elements);
  unsigned int* indexBuffer = malloc(length_of_elements * sizeof(unsigned int));

  for(int i = 0; i < length_of_elements; i++) {
    indexBuffer[i] = getElementAtIndex(head_elements, i);
  }

  glGenVertexArrays(1, &mesh.VAO);
  glGenBuffers(1, &mesh.VBO);
  glGenBuffers(1, &mesh.EBO);

  glBindVertexArray(mesh.VAO);

  glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);
  glBufferData(GL_ARRAY_BUFFER, (8*length_of_verticies) * sizeof(float), vertexArray, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, length_of_elements * sizeof(unsigned int), indexBuffer, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(5 * sizeof(float)));
  glEnableVertexAttribArray(2);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  mesh.num_indicies = length_of_elements * sizeof(unsigned int);

  freeElementTree(head_elements);
  freeVertTree(head_verticies);
  freeVecTree(head_normals);
  free(vertexArray);
  free(indexBuffer);

  printf("Loaded %s\n", file_path);

  return mesh;
}

hge_mesh hgeGenerateQuad() {
  hge_mesh mesh;

  float vertices[] = {                // normals
       // positions       // texture coords
       1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, // top right
       1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom right
      -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom left
      -1.0f,  1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f  // top left
  };

  mesh.num_indicies = 6;
  unsigned int indices[] = {
      0, 1, 3, // first triangle
      1, 2, 3  // second triangle
  };

  glGenVertexArrays(1, &mesh.VAO);
  glGenBuffers(1, &mesh.VBO);
  glGenBuffers(1, &mesh.EBO);

  glBindVertexArray(mesh.VAO);

  glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  // texture coord attribute
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
  // normal attribute
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(5 * sizeof(float)));
  glEnableVertexAttribArray(2);

  glBindVertexArray(0);

  return mesh;
}
