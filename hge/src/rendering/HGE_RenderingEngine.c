#include "HGE_RenderingEngine.h"
#include "HGE_Log.h"
#include <stddef.h>
#include <glad/glad.h>
#include "HGE_ResourceManager.h"

typedef struct {
  hge_shader shader;
  hge_material material;
  hge_mesh mesh;
  hge_transform transform;
  // spritesheet
  bool is_spritesheet;
  hge_vec2 frame_resolution;
  hge_vec2 frame;
} render_data;

#define HGE_MAX_RENDER_QUEUE 500
typedef struct {
  render_data data[HGE_MAX_RENDER_QUEUE];
  int length;
} render_queue;

render_queue rendering_queue;

void hgeClearColor(float r, float g, float b, float a) {
  glClearColor(r, g, b, a);
}

void hgeClear(long int mask) {
  glClear(mask);
}

void hgeRenderInit(hge_window window) {
  HGE_LOG("initializing rendering engine");

  glEnable(GL_DEPTH_TEST);


  hgeResourcesLoadShader("res/shader.vs", NULL, "res/shader.fs", "basic");
  hgeResourcesLoadShader("res/framebuffer.vs", NULL, "res/framebuffer.fs", "framebuffer");

  hge_shader shader = hgeResourcesQueryShader("basic");
  hgeUseShader(shader);
  hgeShaderSetInt(shader, "material.diffuse", 0);
  hgeShaderSetInt(shader, "material.normal", 1);

  HGE_SUCCESS("initialized rendering engine");
}

void hgeRenderFramebufferToScreen(hge_framebuffer framebuffer) {
  hgeUnbindFramebuffer();
  hgeUseShader(hgeResourcesQueryShader("framebuffer"));
  hgeClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  hgeClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  hgeBindTexture(framebuffer.color);
  hgeRenderQuad();
  glBindVertexArray(0);
}

void RenderMesh(hge_mesh mesh) {
  glBindVertexArray(mesh.VAO);
  glDrawElements(GL_TRIANGLES, mesh.num_indicies, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  hgeActiveTexture(0);
}

void hgeRenderQuad() {
  RenderMesh(hgeResourcesQueryMesh("HGE QUAD"));
}

void renderData(render_data data) {

  hge_shader shader = data.shader;
  hge_material material = data.material;
  hge_mesh mesh = data.mesh;
  hge_vec3 position = data.transform.position;
  hge_vec3 scale = data.transform.scale;
  hge_quaternion rotation = data.transform.rotation;

  hgeUseShader(shader);
  hge_mat4 model = hgeMat4(1.0f);
  model = hgeMat4Translate(model, position);

  model = hgeMatMultiply(hgeQuaternionRotationMat4(rotation), model);
  model = hgeMat4Scale(model, scale);
  hgeShaderSetMatrix4(shader, "model", model);

  hgeShaderSetInt(shader, "sprite_size", 0);

  hgeShaderSetBool(shader, "is_spritesheet", data.is_spritesheet);
  if(data.is_spritesheet) {
    hgeShaderSetInt(shader, "frame_resolution_x", data.frame_resolution.x);
    hgeShaderSetInt(shader, "frame_resolution_y", data.frame_resolution.y);
    hgeShaderSetInt(shader, "image_resx", data.material.diffuse.width);
    hgeShaderSetInt(shader, "image_resy", data.material.diffuse.height);
    hgeShaderSetInt(shader, "frame_x", data.frame.x);
    hgeShaderSetInt(shader, "frame_y", data.frame.y);
  }

  hgeShaderSetBool(shader, "material.lit", material.lit);
  hgeShaderSetVec4(shader, "material.color_multiplier", material.color_multiplier);

  hgeActiveTexture(0);
  hgeBindTexture(material.diffuse);
  hgeActiveTexture(1);
  hgeBindTexture(material.normal);

  RenderMesh(mesh);
}

void hgeRenderMesh(hge_shader shader, hge_material material, hge_mesh mesh, hge_transform transform) {
  render_data data = {
    shader,
    material,
    mesh,
    transform,
    false
  };
  rendering_queue.data[rendering_queue.length] = data;
  rendering_queue.length++;
}

void hgeRenderSprite(hge_shader shader, hge_material material, hge_transform transform) {
  render_data data = {
    shader,
    material,
    hgeResourcesQueryMesh("HGE QUAD"),
    transform,
    false
  };
  rendering_queue.data[rendering_queue.length] = data;
  rendering_queue.length++;
}

void hgeRenderSpriteSheet(hge_shader shader, hge_material material, hge_transform transform, hge_vec2 frame_resolution, hge_vec2 frame) {
  render_data data = {
    shader,
    material,
    hgeResourcesQueryMesh("HGE QUAD"),
    transform,
    true,
    frame_resolution,
    frame
  };
  rendering_queue.data[rendering_queue.length] = data;
  rendering_queue.length++;
}

void hgeProcessCamera(hge_camera* camera, hge_transform* transform) {
  hge_vec3 position = transform->position;
  hge_quaternion rotation = transform->rotation;

  hge_mat4 view_matrix;
  hge_mat4 projection_matrix;

  camera->aspect = (float)camera->width / (float)camera->height;

  view_matrix = hgeQuaternionRotationMat4(rotation);
  hge_mat4 translation_matrix = hgeMat4(1.0f);
  //translation_matrix = hgeMat4Translate(translation_matrix, position);
  view_matrix = hgeMat4LookAt(position, hgeVec3Add(position, hgeQuaternionGetFoward(rotation)), hgeQuaternionGetUp(rotation));
  //view_matrix = hgeMatMultiply(translation_matrix, view_matrix);

  if(camera->isOrthographic) projection_matrix = hgeMat4OrthographicProjection(camera->width*camera->fov, camera->height*camera->fov, camera->zNear, camera->zFar);
  else projection_matrix = hgeMat4PerspectiveProjection(hgeRadians(camera->fov), camera->aspect, camera->zNear, camera->zFar);

  for(int i = 0; i < hgeResourcesNumShaders(); i++) {
    hge_resource_shader* shaders = hgeResourcesGetShaderArray();
    hge_shader shader = shaders[i].shader;
    hgeUseShader(shader);
    hgeShaderSetMatrix4(shader, "view", view_matrix);
    hgeShaderSetMatrix4(shader, "projection", projection_matrix);
  }
  glViewport(0, 0, camera->width, camera->height);
}

void hgeRenderFrame() {

  // Send All Light Data
  hgeLightDataReset();
  hge_ecs_request dirlight_request = hgeECSRequest(1, "dirlight");
  for(int i = 0; i < dirlight_request.NUM_ENTITIES; i++) {
    hge_entity* dirlight_entity = dirlight_request.entities[i];
    hge_dirlight* dirlight = dirlight_entity->components[hgeQuery(dirlight_entity, "dirlight")].data;
    hge_system_dirlight(dirlight_entity, dirlight);
  }
  hge_ecs_request pointlight_request = hgeECSRequest(1, "pointlight");
  for(int i = 0; i < pointlight_request.NUM_ENTITIES; i++) {
    hge_entity* pointlight_entity = pointlight_request.entities[i];
    hge_vec3* pointlight_position = pointlight_entity->components[hgeQuery(pointlight_entity, "position")].data;
    hge_pointlight* pointlight = pointlight_entity->components[hgeQuery(pointlight_entity, "pointlight")].data;
    hge_system_pointlight(pointlight_entity, pointlight_position, pointlight);
  }

  // Render Scene for every Camera
  hge_ecs_request camera_request = hgeECSRequest(1, "camera");
  for(int i = 0; i < camera_request.NUM_ENTITIES; i++) {
    hge_entity* camera_entity = camera_request.entities[i];
    hge_camera* camera = camera_entity->components[hgeQuery(camera_entity, "camera")].data;
    hge_transform* camera_transform = camera_entity->components[hgeQuery(camera_entity, "transform")].data;
    hgeBindFramebuffer(camera->framebuffer);
    hgeUseShader(hgeResourcesQueryShader("basic"));
    hgeClearColor(0, 0, 0, 1.0f);
    hgeClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    hgeProcessCamera(camera, camera_transform);

    for(int i = 0; i < rendering_queue.length; i++) {
      renderData(rendering_queue.data[i]);
    }

  }
  glViewport(0, 0, hgeWindowWidth(), hgeWindowHeight());

  rendering_queue.length = 0;
}

void hgeRenderingEngineCleanUp() {
  for(int i = 0; i < hgeResourcesNumMeshes(); i++) {
    hge_mesh mesh = hgeResourcesGetMeshArray()[i].mesh;
    glDeleteVertexArrays(1, &mesh.VAO);
    glDeleteBuffers(1, &mesh.VBO);
    glDeleteBuffers(1, &mesh.EBO);
  }
}
