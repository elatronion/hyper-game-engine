#include "HGE_AudioEngine.h"
#include "HGE_FileUtility.h"
#include "HGE_Log.h"
#include <stddef.h>
#include "AL/al.h"
#include "AL/alc.h"

ALCdevice*	  m_device = NULL;
ALCcontext*	  m_context = NULL;

int hgeAudioInit() {
  HGE_LOG("initializing audio engine...");
  m_device = alcOpenDevice(NULL);
  if (m_device == NULL)
  {
    HGE_ERROR("%s", "Could Not Open Sound Card!");
    return 1;
  }
  m_context = alcCreateContext(m_device, NULL);
  if (m_context == NULL)
  {
    HGE_ERROR("%s", "Could Not Create Context!");
    return 1;
  }
  alcMakeContextCurrent(m_context);
  HGE_SUCCESS("initialized audio engine");
  return 0;
}

void hgeAudioCleanUp() {
  if(m_context) alcDestroyContext(m_context);
  if(m_device) alcCloseDevice(m_device);
}
