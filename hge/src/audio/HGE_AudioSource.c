#include "HGE_AudioSource.h"
#include "AL/al.h"
#include "AL/alc.h"

void hgeAudioSourcePlay(hge_audiosource source) {
  ALenum state;
  alGetSourcei(source.sample.sourceid, AL_SOURCE_STATE, &state);
  alSourcePlay(source.sample.sourceid);
}

void hgeAudioSourceStop(hge_audiosource source) {
  alSourceStop(source.sample.sourceid);
}
