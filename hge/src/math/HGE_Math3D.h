#ifndef HGE_MATH3D_H
#define HGE_MATH3D_H

#include "HGE_Quaternion.h"
#include "HGE_Vector.h"
#include "HGE_Matrix.h"
#include "HGE_Transform.h"
#include <stdbool.h>
#include <math.h>

float hgeRadians(float degrees);

#endif
