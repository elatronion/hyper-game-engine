#ifndef HGE_VECTOR
#define HGE_VECTOR
#include "HGE_MathStructures.h"

hge_vec2 hgeVec2(float x, float y);
hge_vec3 hgeVec3(float x, float y, float z);
hge_vec4 hgeVec4(float x, float y, float z, float w);

hge_vec3 hgeNormalizedDeviceCoords(float mouseX, float mouseY);

hge_vec2 hgeVec2Add(hge_vec2 vecA, hge_vec2 vecB);
hge_vec2 hgeVec2Sub(hge_vec2 vecA, hge_vec2 vecB);
hge_vec2 hgeVec2Mul(hge_vec2 vecA, hge_vec2 vecB);
hge_vec2 hgeVec2Mulf(hge_vec2 vec, float amt);
float hgeVec2Length(hge_vec2 vec2);
hge_vec2 hgeVec2Normalize(hge_vec2 vec2);
float hgeVec2Dot(hge_vec2 vecA, hge_vec2 vecB);

hge_vec3 hgeVec3Add(hge_vec3 vecA, hge_vec3 vecB);
hge_vec3 hgeVec3Sub(hge_vec3 vecA, hge_vec3 vecB);
hge_vec3 hgeVec3Mul(hge_vec3 vecA, hge_vec3 vecB);
hge_vec3 hgeVec3Mulf(hge_vec3 vec, float amt);
float hgeVec3Length(hge_vec3 vec3);
hge_vec3 hgeVec3Normalize(hge_vec3 vec3);
float hgeVec3Dot(hge_vec3 vecA, hge_vec3 vecB);

hge_vec3 hgeVec3Cross();


hge_vec3 hgeVec3RotateAxis(hge_vec3 vec, hge_vec3 axis, float angle);
hge_vec3 hgeVec3Rotate(hge_vec3 vec, hge_quaternion rotation);


#endif
