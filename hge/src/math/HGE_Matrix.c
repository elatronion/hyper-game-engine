#include "HGE_Matrix.h"
#include "HGE_Vector.h"
#include <stdio.h>

hge_mat4 hgeMat4InitRotation(hge_vec3 forward, hge_vec3 up, hge_vec3 right) {
  hge_vec3 f = forward;
  hge_vec3 r = right;
  hge_vec3 u = up;

  hge_mat4 m;
  m.value[0][0] = r.x; m.value[1][0] = r.y; m.value[2][0] = r.z; m.value[3][0] = 0;
  m.value[0][1] = u.x; m.value[1][1] = u.y; m.value[2][1] = u.z; m.value[3][1] = 0;
  m.value[0][2] = f.x; m.value[1][2] = f.y; m.value[2][2] = f.z; m.value[3][2] = 0;
  m.value[0][3] = 0;   m.value[1][3] = 0;   m.value[2][3] = 0;   m.value[3][3] = 1;

  /*
  m.value[0][0] = r.x; m.value[0][1] = r.y; m.value[0][2] = r.z; m.value[0][3] = 0;
  m.value[1][0] = u.x; m.value[1][1] = u.y; m.value[1][2] = u.z; m.value[1][3] = 0;
  m.value[2][0] = f.x; m.value[2][1] = f.y; m.value[2][2] = f.z; m.value[2][3] = 0;
  m.value[3][0] = 0;   m.value[3][1] = 0;   m.value[3][2] = 0;   m.value[3][3] = 1;
  */
  return m;
}

void hgePrintMat4(hge_mat4 mat4) {
  for(int y = 0; y < 4; y++) {
  for(int x = 0; x < 4; x++)
    printf("%f ", mat4.value[x][y]);
  printf("\n");
  }
}

hge_mat4 hgeMat4(float n) {
  hge_mat4 mat4;
  for(int y = 0; y < 4; y++)
  for(int x = 0; x < 4; x++)
  {
    if(x==y)  mat4.value[x][y] = n;
    else      mat4.value[x][y] = 0.0f;
  }
  return mat4;
}

hge_mat4 hgeMatMultiply(hge_mat4 mat4A, hge_mat4 mat4B) {
  hge_mat4 result_mat4;
  for(int x = 0; x < 4; x++)
    for(int y = 0; y < 4; y++)
      result_mat4.value[x][y] = mat4A.value[x][0]*mat4B.value[0][y] +
                                mat4A.value[x][1]*mat4B.value[1][y] +
                                mat4A.value[x][2]*mat4B.value[2][y] +
                                mat4A.value[x][3]*mat4B.value[3][y];
  return result_mat4;
}

hge_mat4 hgeMat4Translate(hge_mat4 mat4, hge_vec3 vec3) {
  hge_mat4 new_mat4 = mat4;
  new_mat4.value[3][0] = vec3.x;
  new_mat4.value[3][1] = vec3.y;
  new_mat4.value[3][2] = vec3.z;
  return new_mat4;
}

hge_mat4 hgeMat4Scale(hge_mat4 mat4, hge_vec3 vec) {
  hge_mat4 scaleMatrix = hgeMat4(1.0f);
  scaleMatrix.value[0][0] = vec.x;
  scaleMatrix.value[1][1] = vec.y;
  scaleMatrix.value[2][2] = vec.z;
  return hgeMatMultiply(scaleMatrix, mat4);
}

hge_mat4 setFrustum(float l, float r, float b, float t, float n, float f)
{
    hge_mat4 mat = hgeMat4(0.0f);
    mat.value[0][0]  = 2 * n / (r - l);
    mat.value[2][0]  = (r + l) / (r - l);
    mat.value[1][1]  = 2 * n / (t - b);
    mat.value[2][1]  = (t + b) / (t - b);
    mat.value[2][2] = -(f + n) / (f - n);
    mat.value[3][2] = (-2 * f * n) / (f - n);
    mat.value[2][3] = -1;
    return mat;
}

hge_mat4 hgeMat4PerspectiveProjection(float fov, float aspect, float zNear, float zFar) {
  bool alt_math = false;
  // MAYBE GOOD?
  if(alt_math) {
    hge_mat4 projection_matrix = hgeMat4(0.0f);
    float f = fov/2;
    projection_matrix.value[0][0] = f/aspect;
    projection_matrix.value[1][1] = f;
    projection_matrix.value[2][2] = (zFar+zNear)/(zNear - zFar);
    projection_matrix.value[3][2] = (2*zFar*zNear)/(zNear-zFar);
    projection_matrix.value[2][3] = -1;
    return projection_matrix;
  }

  float tangent = tanf(fov/2);           // tangent of half fovY
  float height = zNear * tangent;         // half height of near plane
  float width = height * aspect;          // half width of near plane

  // params: left, right, bottom, top, near, far
  return setFrustum(-width, width, -height, height, zNear, zFar);
}

hge_mat4 hgeMat4LookAt(hge_vec3 position, hge_vec3 target, hge_vec3 up) {

  hge_vec3 forward = {position.x-target.x, position.y-target.y, position.z-target.z};
  forward = hgeVec3Normalize(forward);

  hge_vec3 left = hgeVec3Cross(up, forward);
  left = hgeVec3Normalize(left);

  up = hgeVec3Cross(forward, left);

  hge_mat4 matrix = hgeMat4(1.0f);
  matrix.value[0][0] = left.x;
  matrix.value[1][0] = left.y;
  matrix.value[2][0] = left.z;

  matrix.value[0][1] = up.x;
  matrix.value[1][1] = up.y;
  matrix.value[2][1] = up.z;

  matrix.value[0][2] = forward.x;
  matrix.value[1][2] = forward.y;
  matrix.value[2][2] = forward.z;

  matrix.value[3][0] = -left.x * position.x - left.y * position.y - left.z * position.z;
  matrix.value[3][1] = -up.x * position.x - up.y * position.y - up.z * position.z;
  matrix.value[3][2] = -forward.x * position.x - forward.y * position.y - forward.z * position.z;

  return matrix;
}

hge_mat4 hgeMat4OrthographicProjection(float width, float height, float fNear, float fFar) {
  float r = width/2;
  float t = height/2;
  hge_mat4 matrix = hgeMat4(1.0f);
  matrix.value[0][0] = 1/r;
  matrix.value[1][1] = 1/t;
  matrix.value[2][2] = -2/(fFar - fNear);
  matrix.value[3][2] = -((fFar + fNear)/(fFar-fNear));
  return matrix;
}
