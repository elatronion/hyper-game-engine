#ifndef HGE_TRANSFORM
#define HGE_TRANSFORM
#include "HGE_MathStructures.h"

hge_transform hgeTransform(hge_vec3 position, hge_vec3 scale, hge_quaternion rotation);


#endif
