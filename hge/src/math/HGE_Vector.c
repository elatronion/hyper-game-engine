#include "HGE_Vector.h"
#include "HGE_Quaternion.h"
#include "HGE_Window.h"

hge_vec2 hgeVec2(float x, float y) {
  hge_vec2 vec2 = { x, y };
  return vec2;
}

hge_vec3 hgeVec3(float x, float y, float z) {
  hge_vec3 vec3 = { x, y, z };
  return vec3;
}

hge_vec4 hgeVec4(float x, float y, float z, float w) {
  hge_vec4 vec4 = { x, y, z, w };
  return vec4;
}

hge_vec3 hgeNormalizedDeviceCoords(float mouseX, float mouseY) {
  float x = (2.f * mouseX) / hgeWindowWidth() - 1.f;
  float y = (2.f * mouseY) / hgeWindowHeight() - 1.f;
  hge_vec3 normalized_mouse = {x,  -y};
  return normalized_mouse;
}

hge_vec2 hgeVec2Add(hge_vec2 vecA, hge_vec2 vecB) {
  hge_vec2 new_vec = { vecA.x + vecB.x, vecA.y + vecB.y };
  return new_vec;
}

hge_vec2 hgeVec2Sub(hge_vec2 vecA, hge_vec2 vecB) {
  hge_vec2 new_vec = { vecA.x - vecB.x, vecA.y - vecB.y };
  return new_vec;
}

hge_vec2 hgeVec2Mul(hge_vec2 vecA, hge_vec2 vecB) {
  hge_vec2 new_vec = { vecA.x * vecB.x, vecA.y * vecB.y };
  return new_vec;
}

hge_vec2 hgeVec2Mulf(hge_vec2 vec, float amt) {
  hge_vec2 new_vec = { vec.x * amt, vec.y * amt };
  return new_vec;
}

float hgeVec2Length(hge_vec2 vec2) {
  return sqrtf(vec2.x*vec2.x + vec2.y*vec2.y);
}

hge_vec2 hgeVec2Normalize(hge_vec2 vec2) {
  hge_vec2 new_vec2 = vec2;
  float length = hgeVec2Length(vec2);
  if(length == 0) {
	new_vec2.x = 0;
	new_vec2.y = 0;
  } else {
	new_vec2.x = new_vec2.x/length;
	new_vec2.y = new_vec2.y/length;
  }

  return new_vec2;
}

float hgeVec2Dot(hge_vec2 vecA, hge_vec2 vecB) {
  return vecA.x * vecB.x + vecA.y * vecB.y;
}

hge_vec3 hgeVec3Add(hge_vec3 vecA, hge_vec3 vecB) {
  hge_vec3 new_vec = { vecA.x + vecB.x, vecA.y + vecB.y, vecA.z + vecB.z };
  return new_vec;
}

hge_vec3 hgeVec3Sub(hge_vec3 vecA, hge_vec3 vecB) {
  hge_vec3 new_vec = { vecA.x - vecB.x, vecA.y - vecB.y, vecA.z - vecB.z };
  return new_vec;
}

hge_vec3 hgeVec3Mul(hge_vec3 vecA, hge_vec3 vecB) {
  hge_vec3 new_vec = { vecA.x * vecB.x, vecA.y * vecB.y, vecA.z * vecB.z };
  return new_vec;
}

hge_vec3 hgeVec3Mulf(hge_vec3 vec, float amt) {
  hge_vec3 new_vec = { vec.x * amt, vec.y * amt, vec.z * amt };
  return new_vec;
}

float hgeVec3Length(hge_vec3 vec3) {
  return sqrtf(vec3.x*vec3.x + vec3.y*vec3.y + vec3.z*vec3.z);
}

hge_vec3 hgeVec3Normalize(hge_vec3 vec3) {
  hge_vec3 new_vec3 = vec3;
  float length = hgeVec3Length(vec3);
  new_vec3.x = new_vec3.x/length;
  new_vec3.y = new_vec3.y/length;
  new_vec3.z = new_vec3.z/length;
  return new_vec3;
}

float hgeVec3Dot(hge_vec3 vecA, hge_vec3 vecB) {
  return vecA.x * vecB.x + vecA.y * vecB.y + vecA.z * vecB.z;
}

hge_vec3 hgeVec3Cross(hge_vec3 vecA, hge_vec3 vecB) {
  hge_vec3 new_vec =  { (vecA.y * vecB.z) - (vecA.z * vecB.y),
                        (vecA.z * vecB.x) - (vecA.x * vecB.z),
                        (vecA.x * vecB.y) - (vecA.y * vecB.x)
                      };
  return new_vec;
}

hge_vec3 hgeVec3RotateAxis(hge_vec3 vec, hge_vec3 axis, float angle) {
  float sinAngle = (float)sin(-angle);
  float cosAngle = (float)cos(-angle);

  return hgeVec3Cross(vec, hgeVec3Add(hgeVec3Mulf(axis, sinAngle),        // Rotation on local X
    hgeVec3Add(hgeVec3Mulf(vec, cosAngle),                                    // Rotation on local Z
    hgeVec3Mulf(axis, hgeVec3Dot(vec, hgeVec3Mulf(axis, 1 - cosAngle))) // Rotation on local Y
  )));
}

hge_vec3 hgeVec3Rotate(hge_vec3 vec, hge_quaternion rotation) {
  hge_quaternion conjugate = hgeQuaternionConjugate(rotation);
  hge_quaternion w = hgeQuaternionMul(rotation, conjugate);
  return hgeVec3(w.x, w.y, w.z);
}
