#include "HGE_GameMath.h"

bool AABB(hge_transform A, hge_transform B) {
	float Ax = A.position.x - A.scale.x/2;
	float Ay = A.position.y + A.scale.y/2;
	float AX = A.position.x + A.scale.x/2;
	float AY = A.position.y - A.scale.y/2;

	float Bx = B.position.x - B.scale.x/2;
	float By = B.position.y + B.scale.y/2;
	float BX = B.position.x + B.scale.x/2;
	float BY = B.position.y - B.scale.y/2;

	return !(AX < Bx || BX < Ax || AY > By || BY > Ay);
}
