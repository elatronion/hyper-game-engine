#ifndef HGE_MATH_STRUCTURES
#define HGE_MATH_STRUCTURES
#include <stdbool.h>
#include <math.h>

typedef struct {
  float x, y;
} hge_vec2;

typedef struct {
  float x, y, z;
} hge_vec3;

typedef struct {
  float x, y, z, w;
} hge_vec4;

typedef struct {
  float x, y, z, w;
} hge_quaternion;

typedef struct {
  float value[4][4];
} hge_mat4;

typedef struct {
  hge_vec3 position;
  hge_vec3 scale;
  hge_quaternion rotation;
} hge_transform;


#endif
