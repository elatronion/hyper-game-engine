#ifndef HGE_MATRIX
#define HGE_MATRIX
#include "HGE_MathStructures.h"

hge_mat4 hgeMat4InitRotation(hge_vec3 forward, hge_vec3 up, hge_vec3 right);

hge_mat4 hgeMat4(float n);
hge_mat4 hgeMatMultiply(hge_mat4 mat4A, hge_mat4 mat4B);
hge_mat4 hgeMat4Translate(hge_mat4 mat4, hge_vec3 vec3);
hge_mat4 hgeMat4Scale(hge_mat4 mat4, hge_vec3 vec);

hge_mat4 hgeMat4PerspectiveProjection(float fov, float aspect, float fNear, float fFar);
hge_mat4 hgeMat4OrthographicProjection(float width, float height, float fNear, float fFar);

hge_mat4 hgeMat4LookAt(hge_vec3 position, hge_vec3 target, hge_vec3 up);

void hgePrintMat4(hge_mat4 mat4);

#endif
