![Release Version](https://img.shields.io/badge/release-2.0.0-success)  ![License](https://img.shields.io/badge/license-zlib-informational)

# Hyper Game Engine

The Hyper Game Engine is an Open Source ECS OpenGL Game Engine written in C.

## Build Dependencies

- Libraries
	- OpenGL
	- GLFW
	- SOIL
	- GLAD (provided)
	- OpenAL Soft
	- ENet
- Compilers
	- gcc
	- mingw-w64
- Build Automation Tools
	- make

## Installing Dependencies

**Debian**

```bash
sudo apt update && sudo apt install -y build-essential mingw-w64 libgl-dev libglfw3-dev libsoil-dev libenet-dev
```

**Arch**

```bash
sudo pacman -Sy glfw-x11 soil enet & yay -Sy mingw-w64-gcc
```

**Fedora**

```bash
sudo dnf install -y glfw-devel SOIL-devel openal-soft-devel enet-devel mingw32-gcc mingw64-gcc
```

## Build Instructions

As of now, building is only possible on Linux.
To do so, making sure that your current/working directory is the same as the makefile, run the following commands to compile to both Linux and Windows respectively.

```bash
make
```

```bash
make TARGET_OS=windows TARGET_CPU=i686
```

Cross compilation is not supported for different cpu architectures.
If you wish to compile to arm64 or aarch64, run the following command on a system of that architecture.

```bash
make TARGET_CPU=arm64
```

## Installation

On Linux, run the following bash script.

```bash
./install.sh <cpu_arch>
```

This will add the libhypergameengine.so that was compiled by the makefile to your /lib (or /lib64 if present) folder with proper privileges.
It will also create the following folder /usr/include/HGE/ with all the required header files within.

## Usage

Check out [The Basics](docs/The Basics.md)!

## License

Copyright 2020 © Ezra Hradecky

The Hyper Game Engine is available under the [zlib license](https://www.zlib.net/zlib_license.html).

```
This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
```
